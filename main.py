import os
from core import core

from flask.ex.sqlalchemy import SQLAlchemy

#Flask's imports
from flask import render_template
from flask import request


class ConfigClass(object):
	SECRET_KEY = "randomjibber"
	CSRF_ENABLED = True
	SQLALCHEMY_DATABASE_URI = "sqlite:////"


class DevelopConfig(ConfigClass):
	# Class configuration for development
	pass

class DeployConfig(ConfigClass):
	# Class configuration for deployment
	pass

app = core.app
app.root_path = os.path.join(os.path.dirname(__file__))
app.static_folder = os.path.join(
    os.path.realpath(os.path.dirname(__file__)),
    "static"
)

app.config.from_object(DevelopConfig) # Must be set to develop or config accordingly
app.db = SQLAlchemy(app)
core.db = app.db

import blueprints
# register blueprints

for url, blueprint in blueprint.ACTIVE:
	app.register_blueprint(blueprint, url_prefix=url)

if __name__ = "__main__":
	app.db.create_all()
	app.run(debug=True)